angular.module('libraryApp.modal.author', ['ngAnimate', 'ngSanitize', 'ui.bootstrap'])
    .controller('AuthorModalForm', function($uibModal)
    {
        var $authorModal = this;
        $authorModal.open = function(author)
        {
            var modalInstance = $uibModal.open(
            {
                animation: false,
                templateUrl: 'authors/form/author.form.html',
                controller: 'AuthorModalInstanceCtrl',
                controllerAs: '$authorModal',
                resolve:
                {
                    author: function()
                    {
                        return author;
                    }
                }
            });
            modalInstance.result.then(() =>
            {}, () =>
            {});
        };
    });

angular.module('libraryApp.modal.author')
    .controller('AuthorModalInstanceCtrl',
        function($uibModalInstance, author, $http, authorsService)
        {
            var $authorModal = this;
            $authorModal.author = author ? author :
            {};
            $authorModal.title = author ? 'Editar' : 'Adicionar Novo';
            $authorModal.save = (author) =>
            {
                if (author.id)
                {
                    authorsService.update(author);
                }
                else
                {
                    authorsService.create(author);
                }
            };
            $authorModal.reset = function()
            {
                $uibModalInstance.dismiss('cancel');
                $authorModal.author = {};
            };
        });