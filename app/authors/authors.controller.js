'use strict';

angular.module('libraryApp.authors', ['ngRoute', 'angularUtils.directives.dirPagination'])
    .config(['$routeProvider', function($routeProvider)
    {
        $routeProvider.when('/authors',
        {
            templateUrl: 'authors/authors.html',
            controller: 'AuthorsCtrl'
        });
    }])

    .controller('AuthorsCtrl', [
        '$scope', 'authorsService', '$uibModal',
        function($scope, authorsService, $uibModal)
        {
            $scope.orderProp = 'firstName';
            $scope.reverse = false;

            $scope.delete = function(author)
            {
                authorsService.deleteAuthor(author)
                    .then($scope.load);
            };
            $scope.load = function()
            {
                authorsService.getAll()
                    .then(response => $scope.authors = response.data);
            };

            $scope.ordenarPor = function(orderProp)
            {
                $scope.orderProp = orderProp; 
                $scope.reverse = !$scope.reverse;
            }

            $scope.load();
        }
    ]);