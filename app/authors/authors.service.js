angular.module('libraryApp').factory('authorsService', ['$http', function ($http) {
    const url = 'https://bibliapp.herokuapp.com/api/authors';

    var getAll = function () {
        return $http.get(url);
    };

    var update = function (author) {
        return $http.put(url, author).then(
            (response) => {
                console.log(response);
            }
        );
    };

    var create = function (author) {
        return $http.post(url, author).then(
            (response) => {
                console.log(response);
            }
        );
    };
    
    var deleteAuthor = function (author) {
        return $http.delete(`${url}\\${author.id}`);
    };

    return {
        getAll: getAll,
        create: create,
        update: update,
        deleteAuthor: deleteAuthor
    };
}]);