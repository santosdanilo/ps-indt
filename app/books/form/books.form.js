angular.module('libraryApp.modal.book', ['ngAnimate', 'ngSanitize', 'ui.bootstrap','libraryApp'])
    .controller('BookModalForm', function($uibModal)
    {
        var $bookModal = this;
        $bookModal.open = function(book)
        {
            var modalInstance = $uibModal.open(
            {
                animation: false,
                templateUrl: 'books/form/books.form.html',
                controller: 'BookModalInstanceCtrl',
                controllerAs: '$bookModal',
                resolve:
                {
                    book: function()
                    {
                        return book;
                    }
                }
            });
            modalInstance.result.then(() =>
            {}, () =>
            {});
        };
    });

angular.module('libraryApp.modal.book')
    .controller('BookModalInstanceCtrl',
        function($uibModalInstance, book, $http, booksService, authorsService)
        {
            var $bookModal = this;
            $bookModal.book = book ? book :
            {};
            $bookModal.title = book ? 'Editar' : 'Adicionar Novo';
            $bookModal.authors = [];

            authorsService.getAll()
                .then(response => $bookModal.authors = response.data);

            $bookModal.save = (book) =>
            {
                if (book.id)
                {
                    booksService.update(book);
                }
                else
                {
                    booksService.create(book);
                }
                $uibModalInstance.dismiss('cancel');
            };
            $bookModal.reset = function()
            {
                $uibModalInstance.dismiss('cancel');
                $bookModal.book = {};
            };
        });