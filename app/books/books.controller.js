'use strict';

angular.module('libraryApp.books', ['ngRoute', 'angularUtils.directives.dirPagination'])
    .config(['$routeProvider', function($routeProvider)
    {
        $routeProvider.when('/books',
        {
            templateUrl: 'books/books.html',
            controller: 'BooksCtrl'
        });
    }])

    .controller('BooksCtrl', [
        '$scope', 'booksService', '$uibModal',
        function($scope, booksService, $uibModal)
        {
            $scope.orderProp = 'firstName';
            $scope.reverse = false;

            $scope.delete = function(book)
            {
                booksService.deleteBook(book)
                    .then($scope.load);
            };
            $scope.load = function()
            {
                booksService.getAll()
                    .then(response => $scope.books = response.data);
            };

            $scope.ordenarPor = function(orderProp)
            {
                $scope.orderProp = orderProp;
                $scope.reverse = !$scope.reverse; 
            }

            $scope.load();
        }
    ]);