angular.module('libraryApp').factory('booksService', ['$http', function ($http) {
    const url = 'https://bibliapp.herokuapp.com/api/books';

    var getAll = function () {
        return $http.get(url);
    };

    var update = function (book) {
        return $http.put(url, book).then(
            (response) => {
                console.log(response);
            }
        );
    };

    var create = function (book) {
        return $http.post(url, book).then(
            (response) => {
                console.log(response);
            }
        );
    };
    
    var deleteBook = function (book) {
        return $http.delete(`${url}\\${book.id}`);
    };

    return {
        getAll: getAll,
        create: create,
        update: update,
        deleteBook: deleteBook
    };
}]);