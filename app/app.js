'use strict';

// Declare app level module which depends on views, and components
angular.module('libraryApp', [
  'ngRoute',
  'libraryApp.authors',
  'libraryApp.books',
  'libraryApp.modal.author',
  'libraryApp.modal.book'
]).
config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
  $locationProvider.hashPrefix('!');

  $routeProvider.otherwise({
    redirectTo: '/authors'
  });
}]);