Projeto criado com base no [Angular Seed](https://github.com/angular/angular-seed.git)

Para compilar este projeto é necessario ter instalado nodeJS, npm e bower.

Link para instalação de nodeJS e npm: https://nodejs.org/en/download/package-manager

Após ter instalado o npm instale o bower;

```shell
npm install -g bower
```

Após ter instalado nodeJS e bower, executar o seguintes comando para instalar depedencias

```shell
npm install
bower install
```

Tendo baixando todas as depedências, basta executar o servidor e acessar pela url: localhost:8000

```shell
npm start
#localhost:8000
```

